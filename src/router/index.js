import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/Login',
    name: 'Login',
    component: () => import('../views/Login')
  },
  {
    path: '/Contact',
    name: 'Contact',
    component: () => import('../views/Contact')
  },
  {
    path: '/Form',
    name: 'Form',
    component: () => import('../views/Form')
  },
  {
    path: '/Announce',
    name: 'Announce',
    component: () => import('../views/Announce')
  },
  {
    path: '/About',
    name: 'About',
    component: () => import('../views/About')
  },
  {
    path: '/officer',
    name: 'officer',
    component: () => import('../views/officer')
  },
  {
    path: '/professor',
    name: 'professor',
    component: () => import('../views/professor')
  },
  {
    path: '/student',
    name: 'student',
    component: () => import('../views/student')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
